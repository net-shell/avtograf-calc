<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Price;
use App\Order;

class ApiController extends Controller
{
    protected $api;

    public function __construct() {
        $this->middleware('auth');
        $this->api = app('Avtograf\API');
    }

    public function saveOrder() {
        $data = json_decode(request()->input('data'));
        if(!$data) return;
        $order = new Order();
        $order->data = $data;
        $ok = $order->save();
        return response()->json(compact('ok', 'order'));
    }

    public function frameSync() {
        $diff = $this->api->syncFrames();
        return response()->json($diff);
    }

    public function frameImage($name) {
        $name = $this->api->normalizeName($name);
        $url = $this->api->getFrameImageByName($name);
        if(!$url) return;
        return redirect($url);
    }

    public function passepartoutImage($name) {
        $url = $this->api->getPassepartoutImageByName($name);
        if(!$url) return;
        return redirect($url);
    }

    public function passepartout() {
        $data = $this->api->getPassepartouts();
        $search = Request::input('search');
        if($search) {
            $data = array_filter($data, function($item) use ($search) {
                return strpos($item->title, $search) !== false;
            });
        }
        $total = count($data);
        $current_page = request()->input('page', 1);
        $limit = request()->input('limit', 9);
        if($limit) {
            $offset = $limit * ($current_page - 1);
            $data = array_slice($data, $offset, $limit);
        }
        return response()->json(compact('data', 'total', 'current_page', 'search'));
    }

    public function prices() {
        $prices = Price::first();
        return response()->json(compact('prices'));
    }

    public function savePrices() {
        $data = json_decode(request()->input('data'));
        if(!$data) return;
        $price = Price::first();
        $price->data = $data;
        $ok = $price->save();
        return response()->json(compact('ok', 'price'));
    }
}
