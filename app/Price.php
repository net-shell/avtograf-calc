<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $fillable = ['name', 'data'];

    public $timestamps = false;

    public function getDataAttribute() {
        return json_decode($this->attributes['data']);
    }

    public function setDataAttribute($value) {
        $this->attributes['data'] = json_encode($value);
    }
}
