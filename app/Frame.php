<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Frame extends Model
{
    protected $fillable = [
        'name', 'price', 'price_work', 'add_cm', 'active'
    ];

    public function getNameAttribute() {
        $name = $this->attributes['name'];
        return $name[0] .' '. substr($name, 1);
    }

    public function setNameAttribute($value) {
        $this->attributes['name'] = str_replace(' ', '', $value);
    }
}
