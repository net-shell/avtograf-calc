<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['data'];

    public function getDataAttribute() {
        return json_decode($this->attributes['data']);
    }

    public function setDataAttribute($value) {
        $this->attributes['data'] = json_encode($value);
    }
}
