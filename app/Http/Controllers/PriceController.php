<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Frame;
use App\Price;

class PriceController extends Controller
{
    private $order;
    private $prices;
    private $api;

    public function __construct(Request $request) {
        $this->order = json_decode($request->input('order'));
        $this->api = app('Avtograf\API');
        $this->order->frame1 = $this->api->normalizeName($this->order->frame1);
        $this->order->frame2 = $this->api->normalizeName($this->order->frame2);
        $this->prices = Price::first()->data;
    }

    public function calculate()
    {
        if(!$this->order) return;

        $price = array();
        $price['total'] = 0;
        $imageWidth = $this->order->width;
        $imageHeight = $this->order->height;
        $area = $imageWidth * $imageHeight;

        // Passepartout
        if($this->order->passepartout && $this->order->passepartout->width) {
            $offset = $this->order->passepartout->width * 2;
            $imageWidth += $offset;
            $imageHeight += $offset;
            $area = $imageWidth * $imageHeight;
            $price['passpartout'] = ($this->prices->passepartout / 1000) * $area;
            $price['total'] += $price['passpartout'];
        }

        $perimeter = (2 * $imageWidth) + (2 * $imageHeight);
        $perimeterOut = $perimeter;

        // Frame 1
        if($this->order->frame1) {
            $frame = Frame::where('name', $this->order->frame1)->first();
            if($frame) {
                $perimeterOut += $frame->add_cm;
                $price['frame1'] = (($perimeterOut / 100) * $frame->price) + $frame->price_work;
                $price['total'] += $price['frame1'];
            }
        }

        // Frame 2
        if($this->order->frame2) {
            $frame = Frame::where('name', $this->order->frame2)->first();
            if($frame) {
                $perimeterOut += $frame->add_cm;
                $price['frame2'] = (($perimeterOut / 100) * $frame->price) + $frame->price_work;
                $price['total'] += $price['frame2'];
            }
        }

        // Glass
        if($this->order->glass) {
            $glassPrice = 0;
            foreach($this->prices->glass as $record) {
                if($record->name == $this->order->glass) {
                    $glassPrice = $record->price;
                    break;
                }
            }
            $price['glass'] = ($glassPrice / 10000) * $area;
            $price['total'] += $price['glass'];
        }

        // Mirror
        if($this->order->mirror) {
            $price['mirror'] = ($this->prices->mirror / 10000) * $area;
            $price['total'] += $price['mirror'];
        }

        // Back
        if($this->order->back) {
            $price['back'] = ($this->prices->back / 10000) * $area;
            $price['total'] += $price['back'];
        }

        // Hanging
        if($this->order->hanging) {
            $hangingPrice = 0;
            foreach($this->prices->hanging as $record) {
                if($record->name == $this->order->hanging) {
                    $hangingPrice = $record->price;
                    break;
                }
            }
            $price['hanging'] = $hangingPrice;
            $price['total'] += $price['hanging'];
        }

        // Tapestry
        if($this->order->tapestry) {
            $price['tapestry'] = ($this->prices->tapestry / 100) * $perimeter;
            $price['total'] += $price['tapestry'];
        }

        // Subframe
        if($this->order->subframe) {
            $price['subframe'] = ($this->prices->subframe / 100) * $perimeter;
            $price['total'] += $price['subframe'];
        }

        $price['total'] = round($price['total'], 1);
        $price['single'] = $price['total'];
        $price['total'] *= $this->order->quantity;

        $measures = compact('area', 'perimeter', 'perimeterOut');

        return response()->json(compact('price', 'measures'));
    }
}
