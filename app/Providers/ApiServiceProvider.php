<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Avtograf\Api;

class ApiServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->app->bind('Avtograf\API', function($app) {
            return new Api($app);
        });
    }
}
