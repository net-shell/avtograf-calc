@extends('layouts.app')

@section('content')
<div id="admin" class="container">
    <div class="panel panel-default">
        <div class="panel-heading text-center">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#tab1" role="tab" data-toggle="tab">
                        Сглобени рамки
                    </a>
                </li>
                <li role="presentation" v-on:click="getPrices()">
                    <a href="#tab2" role="tab" data-toggle="tab">
                        Консумативи
                    </a>
                </li>
                <li role="presentation" v-on:click="getOrders()">
                    <a href="#tab3" role="tab" data-toggle="tab">
                        Поръчки
                    </a>
                </li>
            </ul>
        </div>
        <div class="panel-body">
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="tab1">
                    <div class="form-inline well">
                        <button id="add-frame" class="btn btn-success">
                            <i class="glyphicon glyphicon-plus"></i>
                            Добавяне на запис
                        </button>
                        <button id="sync-frames" class="btn btn-default">
                            <i class="glyphicon glyphicon-refresh"></i>
                            Синхронизиране
                        </button>
                        <div class="form-group pull-right">
                            <label class="control-label">
                                <input type="text" v-model="search" class="form-control text-right" placeholder="Търсене">
                                <i class="glyphicon glyphicon-search"></i>
                            </label>
                        </div>
                    </div>

                    <p class="text-muted text-center">
                        Показване на @{{ frames.length }} от @{{ pagination.total }} записа:
                    </p>

                    <p class="text-center" v-if="search && search.length">
                        <i class="glyphicon glyphicon-info-sign"></i>
                        Търсите <b>"@{{ search }}"</b>.
                        <a href="#" v-on:click="search = ''">Показване на всички записи</a>
                    </p>

                    <table class="table table-striped table-hover" v-if="frames.length">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Номер</th>
                                <th width="13%">Цена на метър</th>
                                <th width="13%">Цена за труд</th>
                                <th width="13%">Външна обиколка</th>
                                <th>Активна</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="frame in frames" v-bind:class="frame.active ? '' : 'text-muted'">
                                <td class="text-right">
                                    <button v-bind:data-delete="frame.id" class="btn btn-xs btn-danger" title="Изтриване">
                                        <i class="glyphicon glyphicon-trash"></i>
                                    </button>
                                    <button v-bind:data-edit="frame.id" class="btn btn-xs btn-default">
                                        <i class="glyphicon glyphicon-pencil"></i>
                                        Редактиране
                                    </button>
                                </td>
                                <td>
                                    <b class="text-primary">@{{ frame.name }}</b>
                                </td>
                                <td>
                                    <b>@{{ frame.price }}</b>
                                    лв.
                                </td>
                                <td>@{{ frame.price_work }} лв.</td>
                                <td>+@{{ frame.add_cm }} cm</td>
                                <td>
                                    <div v-if="!frame.active">
                                        <i class="glyphicon text-danger glyphicon-remove"></i>
                                        <button v-on:click="toggleFrame(frame)" class="btn btn-default btn-xs">
                                            Активиране
                                        </button>
                                    </div>
                                    <div v-if="frame.active">
                                        <i class="glyphicon glyphicon-ok"></i>
                                        <button v-on:click="toggleFrame(frame)" class="btn btn-default btn-xs">
                                            Изключване
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <div class="form-group" v-if="pagination.hasMore && !loading">
                        <button v-on:click="moreFrames()" class="btn btn-primary btn-lg btn-block">
                            <i class="glyphicon glyphicon-circle-arrow-down"></i>
                            Зареди още резултати
                        </button>
                    </div>

                    <p class="lead text-center" v-if="!frames.length && !loading">
                        Не са намерени записи.
                    </p>

                    <p class="lead text-center" v-if="loading">
                        зареждане...
                    </p>
                </div>

                <!-- Consumables -->
                <div role="tabpanel" class="tab-pane" id="tab2">
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Паспарту</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <input v-model="consumables.passepartout" type="number" class="form-control">
                                            <div class="input-group-addon">
                                                лв./cm<sup>2</sup>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Огледало</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <input v-model="consumables.mirror" type="number" class="form-control">
                                            <div class="input-group-addon">
                                                лв./cm<sup>2</sup>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Гръб</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <input v-model="consumables.back" type="number" class="form-control">
                                            <div class="input-group-addon">
                                                лв./cm<sup>2</sup>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Опъване на гоблен</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <input v-model="consumables.tapestry" type="number" class="form-control">
                                            <div class="input-group-addon">
                                                лв./m
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Подрамка</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <input v-model="consumables.subframe" type="number" class="form-control">
                                            <div class="input-group-addon">
                                                лв./m
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Стъкло</label>
                                    <div class="col-sm-9">
                                        <div class="form-group" v-for="record in consumables.glass">
                                            <div class="col-sm-6">
                                                <input placeholder="Вид стъкло" v-model="record.name" type="text" class="form-control">
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="input-group">
                                                    <input placeholder="Цена" v-model="record.price" type="number" class="form-control">
                                                    <div class="input-group-addon">
                                                        лв./cm<sup>2</sup>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group text-center">
                                            <button type="button" class="btn btn-default" v-on:click="consumables.glass.push({ name: null, price: null })">
                                                <i class="glyphicon glyphicon-plus"></i>
                                                Добавяне
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Окачване</label>
                                    <div class="col-sm-9">
                                        <div class="form-group" v-for="record in consumables.hanging">
                                            <div class="col-sm-6">
                                                <input placeholder="Вид окачване" v-model="record.name" type="text" class="form-control">
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="input-group">
                                                    <input placeholder="Цена" v-model="record.price" type="number" class="form-control">
                                                    <div class="input-group-addon">
                                                        лв./cm<sup>2</sup>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group text-center">
                                            <button type="button" class="btn btn-default" v-on:click="consumables.hanging.push({ name: null, price: null })">
                                                <i class="glyphicon glyphicon-plus"></i>
                                                Добавяне
                                            </button>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="text-center">
                        <button class="btn btn-success" v-on:click="savePrices(this)">
                            <i class="glyphicon glyphicon-ok"></i>
                            Записване на промените
                        </button>
                    </div>
                </div>

                <!-- Orders -->
                <div role="tabpanel" class="tab-pane" id="tab3">
                    <div class="row">
                        <div v-bind:class="activeOrder ? 'col-sm-6' : 'col-sm-12'">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th width="200"></th>
                                        <th>Дата</th>
                                        <th>Клиент</th>
                                        <th>Цена</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="order in orders" v-on:click="activeOrder = order" v-bind:class="activeOrder == order ? 'active' : ''">
                                        <td>
                                            <a class="thumbnail" v-bind:href="'/preview?order=' + JSON.stringify(order.data)" target="_blank">
                                                <img class="img-responsive" v-bind:src="'/preview?order=' + JSON.stringify(order.data)" style="max-width: 200px; max-height: 200px;">
                                            </a>
                                        </td>
                                        <td>@{{ order.created_at }}</td>
                                        <td>@{{ order.data.client }}</td>
                                        <td>@{{ order.data.price.total.toFixed(2) }} лв.</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm-6" v-if="activeOrder">
                            <div class="well">
                                <div class="form-group">
                                    <button class="btn btn-default" v-on:click="activeOrder = null">
                                        <i class="glyphicon glyphicon-remove"></i>
                                        Затвори
                                    </button>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <p>Профил 1: <b>@{{ activeOrder.data.frame1 || "не" }}</b></p>
                                        <p>Профил 2: <b>@{{ activeOrder.data.frame2 || "не" }}</b></p>
                                        <p>Ширина: <b>@{{ activeOrder.data.width ? activeOrder.data.width + " cm" : "няма" }}</b></p>
                                        <p>Височина: <b>@{{ activeOrder.data.height ? activeOrder.data.height + " cm" : "няма" }}</b></p>
                                    </div>
                                    <div class="col-sm-4">
                                        <p>Паспарту: <b>@{{ activeOrder.data.passepartout.width ? activeOrder.data.passepartout.width + " cm" : "не" }}</b></p>
                                        <p>Подрамка: <b>@{{ activeOrder.data.subframe ? "да" : "не" }}</b></p>
                                        <p>Опъване на гоблен: <b>@{{ activeOrder.data.tapestry ? "да" : "не" }}</b></p>
                                        <p>Стъкло: <b>@{{ activeOrder.data.glass || "не" }}</b></p>
                                    </div>
                                    <div class="col-sm-4">
                                        <p>Огледало: <b>@{{ activeOrder.data.mirror ? "да" : "не" }}</b></p>
                                        <p>Гръб: <b>@{{ activeOrder.data.back ? "да" : "не" }}</b></p>
                                        <p>Окачване: <b>@{{ activeOrder.data.hanging ? activeOrder.data.hanging : "не" }}</b></p>
                                        <p>Брой рамки: <b>@{{ activeOrder.data.quantity }}</b></p>
                                    </div>
                                </div>
                                <hr>
                                <div class="text-center">
                                    <p>Ед. цена: <b>@{{ activeOrder.data.price.single.toFixed(2) }} лв.</b></p>
                                    <p class="lead">
                                        Цена:
                                        <b>@{{ activeOrder.data.price.total.toFixed(2) }} лв.</b>
                                    </p>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <p>Рамка 1: <b>@{{ activeOrder.data.price.frame1 || 0 }} лв.</b></p>
                                        <p>Рамка 2: <b>@{{ activeOrder.data.price.frame2 || 0 }} лв.</b></p>
                                        <p>Паспарту: <b>@{{ activeOrder.data.price.passepartout || 0 }} лв.</b></p>
                                    </div>
                                    <div class="col-sm-4">
                                        <p>Стъкло: <b>@{{ activeOrder.data.price.glass || 0 }} лв.</b></p>
                                        <p>Огледало: <b>@{{ activeOrder.data.price.mirror || 0 }} лв.</b></p>
                                        <p>Гръб: <b>@{{ activeOrder.data.price.back || 0 }} лв.</b></p>
                                    </div>
                                    <div class="col-sm-4">
                                        <p>Окачване: <b>@{{ activeOrder.data.price.hanging || 0 }} лв.</b></p>
                                        <p>Опъване на гоблен: <b>@{{ activeOrder.data.price.tapestry || 0 }} лв.</b></p>
                                        <p>Подрамка: <b>@{{ activeOrder.data.price.subframe || 0 }} лв.</b></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="frame-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body" v-if="activeFrame">
                    <p class="lead text-center">
                        Рамка
                    </p>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" :true-value="1" v-model="activeFrame.active">
                                        Активна
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Номер</label>
                                <input type="text" class="form-control" v-model="activeFrame.name">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Цена за метър</label>
                                <div class="input-group">
                                    <input type="number" class="form-control" v-model="activeFrame.price">
                                    <div class="input-group-addon">лв.</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Цена за труд</label>
                                <div class="input-group">
                                    <input type="number" class="form-control" v-model="activeFrame.price_work">
                                    <div class="input-group-addon">лв.</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Външна обиколка</label>
                                <div class="input-group">
                                    <div class="input-group-addon">+</div>
                                    <input type="number" class="form-control" v-model="activeFrame.add_cm">
                                    <div class="input-group-addon">cm</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 text-center">
                            <div class="form-group">
                                <label class="control-label">&nbsp;</label>
                                <button data-dismiss="modal" class="btn btn-default btn-block">
                                    <i class="glyphicon glyphicon-remove"></i>
                                    Затваряне
                                </button>
                            </div>
                            <div class="form-group">
                                <div class="thumbnail">
                                    <img class="img-responsive" v-bind:src="'/api/frame/'+ activeFrame.name +'/image'">
                                </div>
                            </div>
                            <div class="form-group">
                                <button id="save-frame" class="btn btn-success btn-block">
                                    <i class="glyphicon glyphicon-ok"></i>
                                    Записване
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-js')
<script>
var csrfToken = "{{ csrf_token() }}";
var vm = new Vue({
    el: '#admin',
    data: {
        frames: [],
        selectedFrames: [],
        activeFrame: null,
        loading: true,
        search: null,
        pagination: {
            total: 0,
            page: 1,
            hasMore: false
        },
        consumables: {
            passepartout: null,
            glass: [],
            mirror: null,
            back: null,
            hanging: [],
            tapestry: null,
            subframe: null
        },
        orders: [],
        activeOrder: null
    },
    methods: {
        processFramesResponse: function(res) {
            console.log(res)
            if(!res) return;
            if(this.pagination.hasMore) {
                Array.prototype.push.apply(this.frames, res.data);
            } else {
                this.frames = res.data;
            }
            this.pagination.page = res.current_page;
            this.pagination.hasMore = !!res.next_page_url;
            this.pagination.total = res.total;
            if(res.search) {
                this.search = res.search;
            }
            this.loading = false;
        },
        getFrames: function() {
            this.activeFrame = null;
            this.loading = true;
            if(this.xhrFrames) this.xhrFrames.abort();
            this.xhrFrames = $.ajax({
                url: '/api/frame',
                method: 'GET',
                data: { all: true, search: this.search, page: this.pagination.page },
                success: this.processFramesResponse
            });
        },
        moreFrames: function() {
            this.pagination.page++;
            this.getFrames();
        },
        getFrame: function(id) {
            for(var f in this.frames) {
                if(this.frames[f].id == id) {
                    return this.frames[f];
                }
            }
        },
        toggleFrame: function(frame) {
            this.activeFrame = frame;
            this.activeFrame.active = !frame.active ? 1 : null;
            this.saveFrame();
        },
        saveFrame: function(btn) {
            var data = this.activeFrame;
            var url = data.id ? '/api/frame/'+ data.id : '/api/frame';
            data._token = csrfToken;
            if(data.id) data._method = 'put';
            if(btn) btn.attr('disabled', true);
            $.post(url, data, function(res) {
                if(btn) btn.attr('disabled', false);
                $('#frame-modal').modal('hide');
            });
        },
        getPrices: function() {
            var prop = this.consumables;
            $.get('/api/prices', function(res) {
                if(!res || !res.prices) return;
                $.extend(true, prop, res.prices.data);
            });
        },
        savePrices: function(btn) {
            var data = JSON.stringify(this.consumables);
            if(btn) $(btn).attr('disabled', true);
            $.post('/api/prices', { data: data, _token: csrfToken }, function(res) {
                if(btn) $(btn).attr('disabled', false);
            });
        },
        getOrders: function() {
            var me = this;
            $.get('/api/order', function(res) {
                console.log(res)
                if(!res) return;
                me.orders = res;
            });
        }
    },
    watch: {
        search: function() {
            if(this.toSearch) clearTimeout(this.toSearch);
            this.toSearch = setTimeout(this.getFrames, 432);
        }
    }
});

$(document).ready(function() {
    vm.getFrames();

    $('#frame-modal').on('hidden.bs.modal', vm.getFrames)

    $(document).on('click', '#add-frame', function() {
        vm.activeFrame = { name: '', price: '', price_work: '', add_cm: '' };
        $('#frame-modal').modal('show');
    });

    $(document).on('click', '#sync-frames', function() {
        var btn = $(this);
        btn.attr('disabled', true);
        $.get('/api/frame/sync', function(res) {
            btn.attr('disabled', false);
            vm.getFrames();
        });
    });

    $(document).on('click', '#save-frame', function() {
        vm.saveFrame($(this));
    });

    $(document).on('click', 'button[data-edit]', function() {
        var id = $(this).data('edit');
        vm.activeFrame = vm.getFrame(id);
        $('#frame-modal').modal('show');
    });

    $(document).on('click', 'button[data-delete]', function() {
        if(!confirm("Сигурни ли сте, че искате да изтриете този запис?")) return;
        var id = $(this).data('delete');
        $.post('/api/frame/'+ id, { _method: 'delete', _token: csrfToken }, vm.getFrames);
    });
});
</script>
@endsection
