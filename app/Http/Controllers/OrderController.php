<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('store');
    }

    public function index(Request $request)
    {
        $orders = Order::query()->orderBy('created_at', 'desc')->get();
        return response()->json($orders);
    }

    public function store(Request $request)
    {
        $order = new Order();
        $order->data = json_decode($request->input('data'));
        $order->save();
        return response()->json($order);
    }

    public function destroy($id)
    {
        $order = Order::find($id);
        if(!$order) return;
        $order->delete();
    }
}
