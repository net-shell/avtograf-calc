@extends('layouts.app')

@section('navbar')
<ul class="nav navbar-nav navbar-right">
    <li>
        <p class="navbar-btn">
            <a href="{{ url('/') }}" class="btn btn-default">
                <i class="glyphicon glyphicon-repeat"></i>
                Започни отначало
            </a>
        </p>
    </li>
</ul>
@endsection

@section('content')
<div id="calc" class="container">
    <div class="row">
        <div class="col-sm-6" v-if="order.frame1">
            <div>
                <a href="#" title="Виж в голям размер" class="thumbnail user-bg preview-thumbnail" data-toggle="modal" data-target="#preview-modal">
                    <img id="mini-preview" class="live-preview img-responsive">
                </a>
                <div class="well">
                    <div class="form-group text-center">
                        <button id="bg-color" type="button" class="btn btn-default">
                            <i class="user-fg glyphicon glyphicon-tint"></i>
                            Цвят на фона
                        </button>
                    </div>
                    <hr>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3 text-center text-muted">
                                <i v-if="!order.userImage" class="glyphicon glyphicon-picture" style="font-size: 3.14em;"></i>
                                <img v-if="order.userImage" class="img-responsive" v-bind:src="'/uploads/' + order.userImage">
                            </div>
                            <div class="col-sm-9">
                                <label class="control-label">
                                    Качете снимка, за да видите как ще стои в рамката:
                                    </label>
                                <input id="user-image" type="file" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div v-bind:class="order.frame1 ? 'col-sm-6' : 'col-sm-6 col-sm-offset-3'">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#tab1">
                            Размери
                            </a>
                        </h4>
                    </div>
                    <div id="tab1" class="panel-collapse collapse in" role="tabpanel">
                        <div class="panel-body form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Широчина</label>
                                <div class="col-sm-7">
                                    <div class="input-group">
                                        <input type="number" min="1" class="form-control" v-model="order.width">
                                        <div class="input-group-addon">cm</div>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <a href="#" title="Това е ширината на вашата картина, гоблен, огледало - в сантиметри" data-toggle="tooltip">
                                        <i class="glyphicon glyphicon-info-sign text-info"></i>
                                    </a>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Височина</label>
                                <div class="col-sm-7">
                                    <div class="input-group">
                                        <input type="number" min="1" class="form-control" v-model="order.height">
                                        <div class="input-group-addon">cm</div>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <a href="#" title="Това е височината на вашата картина, гоблен, огледало - в сантиметри" data-toggle="tooltip">
                                        <i class="glyphicon glyphicon-info-sign text-info"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel" v-bind:class="!order.frame1 ? 'panel-primary' : 'panel-default'">
                    <div class="panel-heading" role="tab">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#tab2">
                            Рамка 1
                            </a>
                        </h4>
                    </div>
                    <div id="tab2" class="panel-collapse collapse in" role="tabpanel">
                        <div class="panel-body">
                            <div class="form-group">
                                <input class="form-control" v-model="frames1search" placeholder="Търсене">
                            </div>
                            <div class="row">
                                <div v-for="frame in frames1" class="col-sm-4 text-center">
                                    <div class="frame-item" v-bind:class="frame.name == order.frame1 ? 'active' : ''" v-on:click="order.frame1 = frame.name;">
                                        <div style="padding-top: .5em;">
                                            <img v-bind:src="'/api/frame/'+ frame.name +'/image'" width="100">
                                        </div>
                                        <label class="control-label">@{{ frame.name }}</label>
                                    </div>
                                </div>
                            </div>
                            <p class="text-center" v-if="frames1search && frames1search.length">
                                <i class="glyphicon glyphicon-info-sign"></i>
                                Търсите <b>"@{{ frames1search }}"</b>.
                                <a href="#" v-on:click="frames1search = ''">Показване на всички</a>
                            </p>
                            <div class="well">
                                <div class="row text-center">
                                    <div class="col-sm-4">
                                        <button class="btn btn-default" v-on:click="prevFramePage(1)">
                                            <i class="glyphicon glyphicon-chevron-left"></i>
                                            Предишна
                                        </button>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="control-label" style="padding: .7em 0;">
                                            страница
                                            <b>@{{ frames1pagination.page }}</b>
                                            от
                                            @{{ frames1pagination.pages }}
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <button class="btn btn-default" v-on:click="nextFramePage(1)">
                                            Следваща
                                            <i class="glyphicon glyphicon-chevron-right"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default" v-if="order.frame1">
                    <div class="panel-heading" role="tab">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#tab3">
                            Рамка 2
                            </a>
                        </h4>
                    </div>
                    <div id="tab3" class="panel-collapse collapse" role="tabpanel">
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="row">
                                    <div v-bind:class="order.frame2 ? 'col-sm-6' : 'col-sm-12'">
                                        <input class="form-control" v-model="frames2search" placeholder="Търсене">
                                    </div>
                                    <div class="col-sm-6" v-if="order.frame2">
                                        <button class="btn text-danger btn-block" v-on:click="order.frame2 = ''; frames2search = '';">
                                            <i class="glyphicon glyphicon-trash"></i>
                                            Махни избраната рамка
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div v-for="frame in frames2" class="col-sm-4 text-center">
                                    <div class="frame-item" v-bind:class="frame.name == order.frame2 ? 'active' : ''" v-on:click="order.frame2 = frame.name;">
                                        <div style="padding-top: .5em;">
                                            <img v-bind:src="'/api/frame/'+ frame.name +'/image'" width="100">
                                        </div>
                                        <label class="control-label">@{{ frame.name }}</label>
                                    </div>
                                </div>
                            </div>
                            <p class="text-center" v-if="frames2search && frames2search.length">
                                <i class="glyphicon glyphicon-info-sign"></i>
                                Търсите <b>"@{{ frames2search }}"</b>.
                                <a href="#" v-on:click="frames2search = ''">Показване на всички</a>
                            </p>
                            <div class="well">
                                <div class="row text-center">
                                    <div class="col-sm-4">
                                        <button class="btn btn-default" v-on:click="prevFramePage(2)">
                                            <i class="glyphicon glyphicon-chevron-left"></i>
                                            Предишна
                                        </button>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="control-label" style="padding: .7em;">
                                            страница
                                            <b>@{{ frames2pagination.page }}</b>
                                            от
                                            @{{ frames2pagination.pages }}
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <button class="btn btn-default" v-on:click="nextFramePage(2)">
                                            Следваща
                                            <i class="glyphicon glyphicon-chevron-right"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default" v-if="order.frame1 || order.frame2">
                    <div class="panel-heading" role="tab">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#tab4">
                            Паспарту (картонено)
                            </a>
                        </h4>
                    </div>
                    <div id="tab4" class="panel-collapse collapse" role="tabpanel">
                        <div class="panel-body form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Широчина</label>
                                <div class="col-sm-7">
                                    <div class="input-group">
                                        <input type="number" min="0" class="form-control" v-model="order.passepartout.width">
                                        <div class="input-group-addon">cm</div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input class="form-control" v-model="passepartoutSearch" placeholder="Търсене">
                                </div>
                            </div>
                            <div class="row">
                                <div v-for="passepartout in passepartouts" class="col-sm-4 text-center">
                                    <div class="frame-item" v-bind:class="passepartout.title == order.passepartout.name ? 'active' : ''" v-on:click="order.passepartout.name = passepartout.title;">
                                        <div style="padding-top: .5em;">
                                            <img v-bind:src="'/api/passepartout/'+ passepartout.title +'/image'" width="100">
                                        </div>
                                        <div class="lead">@{{ passepartout.title }}</div>
                                    </div>
                                </div>
                            </div>
                            <p class="text-center" v-if="passepartoutSearch && passepartoutSearch.length">
                                <i class="glyphicon glyphicon-info-sign"></i>
                                Търсите <b>"@{{ passepartoutSearch }}"</b>.
                                <a href="#" v-on:click="passepartoutSearch = ''">Показване на всички</a>
                            </p>
                            <div class="well">
                                <div class="row text-center">
                                    <div class="col-sm-4">
                                        <button class="btn btn-default" v-on:click="prevPassepartoutPage()">
                                            <i class="glyphicon glyphicon-chevron-left"></i>
                                            Предишна
                                        </button>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="control-label" style="padding: .7em;">
                                            страница
                                            <b>@{{ passepartoutPagination.page }}</b>
                                            от
                                            @{{ passepartoutPagination.pages }}
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <button class="btn btn-default" v-on:click="nextPassepartoutPage()">
                                            Следваща
                                            <i class="glyphicon glyphicon-chevron-right"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default" v-if="order.frame1 || order.frame2">
                    <div class="panel-heading" role="tab">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#tab5">
                            Стъкло
                            </a>
                        </h4>
                    </div>
                    <div id="tab5" class="panel-collapse collapse" role="tabpanel">
                        <div class="panel-body row">
                            <div class="col-sm-11">
                                <select class="form-control" v-model="order.glass">
                                    <option>не</option>
                                    <option v-for="record in consumables.glass">@{{ record.name }}</option>
                                </select>
                            </div>
                            <div class="col-sm-1">
                                <a href="#" title="Не се поставя върху маслени картини или релефно пано!" data-toggle="tooltip">
                                    <i class="glyphicon glyphicon-info-sign text-info"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default" v-if="order.frame1 || order.frame2">
                    <div class="panel-heading" role="tab">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#tab6">
                            Огледало
                            </a>
                        </h4>
                    </div>
                    <div id="tab6" class="panel-collapse collapse" role="tabpanel">
                        <div class="panel-body row">
                            <div class="col-sm-11 checkbox">
                                <label>
                                    <input type="checkbox" v-model="order.mirror">
                                    Огледало
                                </label>
                            </div>
                            <div class="col-sm-1">
                                <a href="#" title="Дебелина 3мм /без фасет/" data-toggle="tooltip">
                                    <i class="glyphicon glyphicon-info-sign text-info"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default" v-if="order.frame1 || order.frame2">
                    <div class="panel-heading" role="tab">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#tab7">
                            Гръб
                            </a>
                        </h4>
                    </div>
                    <div id="tab7" class="panel-collapse collapse" role="tabpanel">
                        <div class="panel-body row">
                            <div class="col-sm-11 checkbox">
                                <label>
                                    <input type="checkbox" v-model="order.back">
                                    Гръб
                                </label>
                            </div>
                            <div class="col-sm-1">
                                <a href="#" title="Отбележете само когато рамката е със стъкло." data-toggle="tooltip">
                                    <i class="glyphicon glyphicon-info-sign text-info"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default" v-if="order.frame1 || order.frame2">
                    <div class="panel-heading" role="tab">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#tab8">
                            Окачване
                            </a>
                        </h4>
                    </div>
                    <div id="tab8" class="panel-collapse collapse" role="tabpanel">
                        <div class="panel-body row">
                            <div class="col-sm-11">
                                <select class="form-control" v-model="order.hanging">
                                    <option>не</option>
                                    <option v-for="record in consumables.hanging">@{{ record.name }}</option>
                                </select>
                            </div>
                            <div class="col-sm-1">
                                <a href="#" title="Видът на окачващите елементи се определя от размера и вида на рамката." data-toggle="tooltip">
                                    <i class="glyphicon glyphicon-info-sign text-info"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default" v-if="order.frame1 || order.frame2">
                    <div class="panel-heading" role="tab">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#tab9">
                            Опъване на гоблен
                            </a>
                        </h4>
                    </div>
                    <div id="tab9" class="panel-collapse collapse" role="tabpanel">
                        <div class="panel-body row">
                            <div class="checkbox col-sm-11">
                                <label>
                                    <input type="checkbox" v-model="order.tapestry">
                                    Опъване на гоблен
                                </label>
                            </div>
                            <div class="col-sm-1">
                                <a href="#" title="Гобленът се опъва прецизно на твърда подложка, без да се наранява материята." data-toggle="tooltip">
                                    <i class="glyphicon glyphicon-info-sign text-info"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default" v-if="order.frame1 || order.frame2">
                    <div class="panel-heading" role="tab">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#tab10">
                            Подрамка
                            </a>
                        </h4>
                    </div>
                    <div id="tab10" class="panel-collapse collapse" role="tabpanel">
                        <div class="panel-body row">
                            <div class="checkbox col-sm-11">
                                <label>
                                    <input type="checkbox" v-model="order.subframe">
                                    Подрамка
                                </label>
                            </div>
                            <div class="col-sm-1">
                                <a href="#" title="Ако вашата картина е рисувана на платно и няма подрамка, ние можем да изработим такава и да изпънем платното." data-toggle="tooltip">
                                    <i class="glyphicon glyphicon-info-sign text-info"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default" v-if="order.frame1 || order.frame2">
                    <div class="panel-heading" role="tab">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#tab11">
                            Брой рамки
                            </a>
                        </h4>
                    </div>
                    <div id="tab11" class="panel-collapse collapse" role="tabpanel">
                        <div class="panel-body">
                            <input type="number" min="1" class="form-control" v-model="order.quantity">
                        </div>
                    </div>
                </div>

                <div class="panel panel-default" v-if="order.frame1 || order.frame2">
                    <div class="panel-heading" role="tab">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#tab12">
                            Клиент
                            </a>
                        </h4>
                    </div>
                    <div id="tab12" class="panel-collapse collapse" role="tabpanel">
                        <div class="panel-body form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Име</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" v-model="order.client.name">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Адрес</label>
                                <div class="col-sm-7">
                                    <textarea class="form-control" v-model="order.client.address"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Email</label>
                                <div class="col-sm-7">
                                    <input type="email" class="form-control" v-model="order.client.email">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Телефон</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" v-model="order.client.phone">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Допълнителна информация</label>
                                <div class="col-sm-7">
                                    <textarea class="form-control" v-model="order.client.notes"></textarea>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="panel panel-default" v-if="order.frame1 || order.frame2">
                    <div class="panel-heading" role="tab">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#tab13">
                            Цени и доставка
                            </a>
                        </h4>
                    </div>
                    <div id="tab13" class="panel-collapse collapse" role="tabpanel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <p>Профил 1: <b>@{{ order.frame1 || "не" }}</b></p>
                                    <p>Профил 2: <b>@{{ order.frame2 || "не" }}</b></p>
                                    <p>Ширина: <b>@{{ order.width ? order.width + " cm" : "няма" }}</b></p>
                                    <p>Височина: <b>@{{ order.height ? order.height + " cm" : "няма" }}</b></p>
                                    <p>Паспарту: <b>@{{ order.passepartout.width ? order.passepartout.width + " cm" : "не" }}</b></p>
                                    <p>Подрамка: <b>@{{ order.subframe ? "да" : "не" }}</b></p>
                                    <p>Опъване на гоблен: <b>@{{ order.tapestry ? "да" : "не" }}</b></p>
                                </div>
                                <div class="col-sm-6">
                                    <p>Стъкло: <b>@{{ order.glass || "не" }}</b></p>
                                    <p>Огледало: <b>@{{ order.mirror ? "да" : "не" }}</b></p>
                                    <p>Гръб: <b>@{{ order.back ? "да" : "не" }}</b></p>
                                    <p>Окачване: <b>@{{ order.hanging ? order.hanging : "не" }}</b></p>
                                    <p>Брой рамки: <b>@{{ order.quantity }}</b></p>
                                    <p>Ед. цена: <b>@{{ price.single.toFixed(2) }} лв.</b></p>
                                    <p class="lead">
                                        Цена:
                                        <b>@{{ price.total.toFixed(2) }} лв.</b>
                                    </p>
                                </div>
                            </div>
                            <hr>
                            <div class="row text-info">
                                <div class="col-sm-6">
                                    <p>Площ: <b>@{{ measures.area }} cm<sup>2</sup></b></p>
                                    <p>Обиколка: <b>@{{ measures.perimeter }} cm</b></p>
                                    <p>Външна обиколка: <b>@{{ measures.perimeterOut }} cm</b></p>
                                </div>
                                <div class="col-sm-6">
                                    <p>Рамка 1: <b>@{{ price.frame1 || 0 }} лв.</b></p>
                                    <p>Рамка 2: <b>@{{ price.frame2 || 0 }} лв.</b></p>
                                    <p>Паспарту: <b>@{{ price.passepartout || 0 }} лв.</b></p>
                                    <p>Стъкло: <b>@{{ price.glass || 0 }} лв.</b></p>
                                    <p>Огледало: <b>@{{ price.mirror || 0 }} лв.</b></p>
                                    <p>Гръб: <b>@{{ price.back || 0 }} лв.</b></p>
                                    <p>Окачване: <b>@{{ price.hanging || 0 }} лв.</b></p>
                                    <p>Опъване на гоблен: <b>@{{ price.tapestry || 0 }} лв.</b></p>
                                    <p>Подрамка: <b>@{{ price.subframe || 0 }} лв.</b></p>
                                </div>
                            </div>
                            <hr>
                            <p class="lead text-primary text-center">ПРИ ПОРЪЧКА ЗА НАД 10 БРОЯ РАМКИ, ОБАДЕТЕ СЕ ЗА СПЕЦИАЛНА ЦЕНА!</p>
                            <button onclick="saveOrder(this);" class="btn btn-success btn-lg btn-block">
                                <i class="glyphicon glyphicon-ok"></i>
                                Поръчай
                            </button>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default" v-if="!order.frame1 && !order.frame2">
                    <div class="panel-body">
                        <p class="lead text-center">
                            <i class="glyphicon glyphicon-circle-arrow-up"></i>
                            <br>
                            Изберете рамка, за да видите повече опции и цена.
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="preview-modal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document" style="width: 92%;">
		<div class="modal-content user-bg">
			<div class="modal-body">
				<img class="live-preview img-responsive" style="margin: 0 auto;">
			</div>
		</div>
	</div>
</div>
@endsection

@section('page-js')
<style>
#calc .glyphicon {
    font-size: 1.2em;
}
.preview-thumbnail {
    position: relative;
    padding: 1em;
}
</style>

<script>
var csrfToken = "{{ csrf_token() }}";
var vm = new Vue({
    el: '#calc',
    data: {
        order: {
            width: 12,
            height: 9,
            frame1: null,
            frame2: null,
            passepartout: {
                width: null,
                name: null
            },
            glass: null,
            mirror: null,
            back: null,
            hanging: null,
            tapestry: null,
            subframe: null,
            quantity: 1,
            client: {
                name: null,
                address: null,
                email: null,
                phone: null,
                notes: null
            },
            userImage: null
        },
        price: {
            single: 0,
            total: 0
        },
        measures: {
            area: 0,
            perimeter: 0,
            perimeterOut: 0
        },
        frames1: [],
        frames1search: '',
        frames1pagination: {
            page: 1,
            pages: 1,
            total: 1
        },
        frames2: [],
        frames2search: '',
        frames2pagination: {
            page: 1,
            pages: 1,
            total: 1
        },
        passepartouts: [],
        passepartoutSearch: '',
        passepartoutPagination: {
            page: 1,
            pages: 1,
            total: 1
        },
        consumables: {
            passepartout: null,
            glass: [],
            mirror: null,
            back: null,
            hanging: [],
            tapestry: null,
            subframe: null
        }
    },
    watch: {
        'order': {
            deep: true,
            handler: refreshPrice
        },
        'frames1search': function(newVal) {
            if(newVal) newVal = newVal.replace(/[^A-Z0-9]/g, '');
            refreshFrames('1');
        },
        'frames2search': function() {
            refreshFrames('2');
        },
        'passepartoutSearch': refreshPassepartout,
        'order.width': refreshPreview,
        'order.height': refreshPreview,
        'order.frame1': function() {
            setTimeout(initFull, 30)
            refreshPreview();
        },
        'order.frame2': refreshPreview,
        'order.passepartout.width': refreshPreview,
        'order.passepartout.name': refreshPreview,
        'order.userImage': refreshPreview
    },
    methods: {
        nextFramePage: function(fset) {
            var key = 'frames'+ fset +'pagination';
            if(this[key].page == this[key].pages) return;
            this[key].page++;
            refreshFrames(fset);
        },
        prevFramePage: function(fset) {
            var key = 'frames'+ fset +'pagination';
            if(this[key].page == 1) return;
            this[key].page--;
            refreshFrames(fset);
        },
        nextPassepartoutPage: function() {
            if(this.passepartoutPagination.page == this.passepartoutPagination.pages) return;
            this.passepartoutPagination.page++;
            refreshPassepartout();
        },
        prevFramePage: function(fset) {
            if(this.passepartoutPagination.page == 1) return;
            this.passepartoutPagination.page--;
            refreshPassepartout();
        }
    }
});

function refreshFrames(fset) {
    var key = 'frames' + fset;
    var limit = 9;
    var search = vm[key + 'search'];
    var pagination = vm[key + 'pagination'];
    if(vm[key + 'xhr']) vm[key + 'xhr'].abort();
    vm[key + 'xhr'] = $.ajax({
        url: '/api/frame',
        method: 'GET',
        data: { search: search, limit: limit, page: pagination.page },
        success: function(res) {
            if(!res) return;
            pagination.page = res.current_page;
            pagination.pages = Math.ceil(res.total / limit);
            pagination.total = res.total;
            if(res.search) {
                vm[key + 'search'] = res.search;
            }
            vm[key] = res.data;
        }
    });
}

function refreshPassepartout() {
    var limit = 9;
    var search = vm.passepartoutSearch;
    var pagination = vm.passepartoutPagination;
    if(vm.passepartoutXhr) vm.passepartoutXhr.abort();
    vm.passepartoutXhr = $.ajax({
        url: '/api/passepartout',
        method: 'GET',
        data: { search: search, limit: limit, page: pagination.page },
        success: function(res) {
            console.log(res)
            if(!res) return;
            pagination.page = res.current_page;
            pagination.pages = Math.ceil(res.total / limit);
            pagination.total = res.total;
            if(res.search) {
                vm.passepartoutSearch = res.search;
            }
            vm.passepartouts = res.data;
        }
    });
}

function refreshPreview() {
    setTimeout(function() {
        $('img.live-preview').attr('src', null).attr('src', '/preview?order=' + JSON.stringify(vm.order));
    }, 30);
}

function refreshPrice() {
    $.get('/price?order=' + JSON.stringify(vm.order), function(res) {
        vm.price = res.price;
        vm.measures = res.measures;
    });
}

function getPriceData() {
    var prop = vm.consumables;
    $.get('/api/prices', function(res) {
        $.extend(true, prop, res.prices.data);
    });
}

function saveOrder(btn) {
    var data = vm.order;
    data.price = vm.price;
    data = JSON.stringify(data);
    if(btn) $(btn).attr('disabled', true);
    $.post('/api/order', { data: data, _token: csrfToken }, function(res) {
        console.log(res);
    });
}

$(document).ready(function() {
    refreshFrames(1);
    refreshFrames(2);
    refreshPassepartout();
    getPriceData();
    initFull();

    // Upload user image
    $(document).on('change', '#user-image', function() {
        var formData = new FormData();
        formData.append('image', this.files[0]);
        formData.append('_token', csrfToken);
        $.ajax({
            url : '/upload',
            type : 'POST',
            data : formData,
            processData: false,
            contentType: false,
            success : function(data) {
                if(!data || !data.newName) return;
                vm.order.userImage = data.newName;
            }
        });
    });
});

function initFull() {
    // Fit the preview in a rectangle
    $('#mini-preview').css('max-height', $('#mini-preview').parent().width() + 'px');

    // Generate preview
    refreshPreview();

    // Enable tooltips
    $('[data-toggle="tooltip"]').tooltip({ placement: 'bottom' });

    // Enable color picker
    $('#bg-color').colorpicker({
            format: 'hex'
        }).on('changeColor', function(e) {
            var color = e.color.toString('rgba');
            $('.user-bg').css('background-color', color);
            $('.user-fg').css('color', color);
        });
}

$(window).on('load resize', initFull);
</script>
@endsection
