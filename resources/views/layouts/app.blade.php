<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.1/css/bootstrap-colorpicker.min.css" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    @if(Auth::check())
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                            <span class="sr-only">Toggle Navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    @endif

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <img src="/img/logo.png" style="max-height: 4em; display: inline; vertical-align: middle;">
                        / калкулатор
                    </a>
                </div>

                @if(Auth::check())
                    <div class="collapse navbar-collapse" id="app-navbar-collapse" style="padding: 1.8em;">
                        <ul class="nav navbar-nav">
                            <li class="navbar-text">
                                <b>
                                    <i class="glyphicon glyphicon-user"></i>
                                    {{ Auth::user()->name }}
                                </b>
                            </li>
                            <li class="{{ app('request')->is('admin') ? 'active' : '' }}">
                                <a href="{{ url('/admin') }}">
                                    <i class="glyphicon glyphicon-cog"></i>
                                    Администрация
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    <i class="glyphicon glyphicon-log-out"></i>
                                    Изход
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                        @yield('navbar')
                    </div>
                @endif
            </div>
        </nav>

        <div style="padding-top: 2em;">
            @yield('content')
        </div>
    </div>

    @if(Auth::guest())
        <nav class="navbar navbar-default">
            <div class="container">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="{{ route('login') }}">
                            <i class="glyphicon glyphicon-log-in"></i>
                            Вход
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('register') }}">
                            <i class="glyphicon glyphicon-thumbs-up"></i>
                            Регистрация
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="container">
            <p class="text-muted text-right">Изработено с внимание от netshell.</p>
        </div>
    @endif

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.1/js/bootstrap-colorpicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.2.6/vue.min.js"></script>
    <!-- <script src="{{ asset('js/app.js') }}"></script> -->
    @yield('page-js')
</body>
</html>
