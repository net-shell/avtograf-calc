<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Image;

class UploadController extends Controller
{
    public function upload()
    {
        if(!isset($_FILES['image']) || !count($_FILES['image'])) return;
        $file = $_FILES['image']['tmp_name'];
        if(!$file) return;
        $ext = exif_imagetype($file);
        $ext = $ext == IMAGETYPE_PNG ? 'png' : ($ext == IMAGETYPE_JPEG ? 'jpg' : ($ext == IMAGETYPE_GIF ? 'gif' : false));
        if(!$ext) return;
        $newName = sha1_file($file) . '.' . $ext;
        $dest = public_path('uploads/' . $newName);
        Image::make($file)->fit(1000, 1000)->save($dest);
        return response()->json(compact('newName'));
    }
}
