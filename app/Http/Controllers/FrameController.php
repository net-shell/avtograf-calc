<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Frame;

class FrameController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index');
    }

    public function index(Request $request)
    {
        $frames = Frame::where('active', 1);
        if($request->get('all')) {
            $frames = Frame::query();
        }
        // Apply search
        $search = $request->get('search');
        $search = strtoupper($search);
        $search = preg_replace("/[^\-A-Z0-9]+/", '', $search);
        if(strlen($search)) {
            $frames->where('name', 'LIKE', "%$search%");
        }
        // Apply pagination
        $limit = $request->get('limit', 32);
        $frames = $frames->simplePaginate($limit)->toArray();
        $frames['total'] = Frame::count();
        $frames['search'] = $search;
        return response()->json($frames);
    }

    public function store(Request $request)
    {
        $frame = new Frame();
        $frame->fill($request->only(['name', 'price', 'price_work', 'add_cm', 'active']));
        $frame->save();
        return response()->json($frame);
    }

    public function update($id, Request $request)
    {
        $frame = Frame::find($id);
        if(!$frame) return;
        $frame->fill($request->only(['name', 'price', 'price_work', 'add_cm', 'active']));
        $frame->save();
        return response()->json($frame);
    }

    public function destroy($id)
    {
        $frame = Frame::find($id);
        if(!$frame) return;
        $frame->delete();
    }
}
