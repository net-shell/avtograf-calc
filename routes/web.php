<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('calculator');
});

Route::get('/preview', 'PreviewController@render');
Route::get('/price', 'PriceController@calculate');
Route::post('/upload', 'UploadController@upload');

Auth::routes();

Route::get('/admin', 'AdminController@index');

Route::get('/api/frame/sync', 'ApiController@frameSync');
Route::get('/api/frame/{name}/image', 'ApiController@frameImage');
Route::get('/api/passepartout/{name}/image', 'ApiController@passepartoutImage');
Route::get('/api/passepartout', 'ApiController@passepartout');
Route::get('/api/prices', 'ApiController@prices');
Route::post('/api/prices', 'ApiController@savePrices');

Route::resource('/api/order', 'OrderController');

Route::resource('/api/frame', 'FrameController');