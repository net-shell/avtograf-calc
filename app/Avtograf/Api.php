<?php

namespace App\Avtograf;

use GuzzleHttp\Client as HttpClient;
use Illuminate\Support\Facades\Cache;
use App\Frame;
use DB;

class Api
{
    private $app;
    private $http;
    private $categoryFrame = 13;
    private $categoryPassepartout = 4;
    private $cacheTtl = 60 * 24;
    private $baseUrl = 'http://avtograf.kz.archimed.bg/api';
    private $imageBase = 'http://avtograf.kz.archimed.bg/uploads/product/';

    public function __construct($app) {
        $this->app = $app;
        $this->http = new HttpClient();
    }

    public function syncFrames() {
        $updateNames = [];
        Cache::forget('api_products');
        foreach($this->getFramesCategory() as $frame) {
            $name = $this->normalizeName($frame->title);
            $updateNames[] = $name;
        }
        Frame::query()->update(['active' => null]);
        Frame::whereIn('name', $updateNames)->update(['active' => 1]);
        dd($updateNames);
    }

    public function getFrames() {
        return Cache::remember('api_products', $this->cacheTtl, function() {
            $res = $this->http->get($this->baseUrl .'/all');
            return json_decode($res->getBody());
        });
    }
    

    public function getFrameById($id) {
        $frames = $this->getFrames();
        foreach($frames as $f => $frame) {
            if($frame->id == $id && $frame->category_id == $this->categoryFrame)
                return $frame;
        }
    }

    public function getPassepartouts() {
        $frames = $this->getFrames();
        $result = array();
        foreach($frames as $f => $frame) {
            if($frame->category_id == $this->categoryPassepartout)
                $result[] = $frame;
        }
        return $result;
    }

    public function getFramesCategory() {
        $frames = $this->getFrames();
        $result = array();
        foreach($frames as $f => $frame) {
            if($frame->category_id == $this->categoryFrame)
                $result[] = $frame;
        }
        return $result;
    }

    public function getFrameByName($name) {
        $frames = $this->getFrames();
        foreach($frames as $f => $frame) {
            $title = $this->normalizeName($frame->title);
            if($title == $name && $frame->category_id == $this->categoryFrame)
                return $frame;
        }
    }

    public function getPassepartoutByName($name) {
        $frames = $this->getFrames();
        foreach($frames as $f => $frame) {
            $title = $this->normalizeName($frame->title);
            if($title == $name && $frame->category_id == $this->categoryPassepartout)
                return $frame;
        }
    }

    public function getFrameImage($frame) {
        if(!$frame || !$frame->images || !count($frame->images)) return;
        $url = $this->imageBase . $frame->images[0]->filename;
        return $url;
    }

    public function getPassepartoutImageByName($name) {
        return Cache::remember('api_passepartout_img_'. $name, $this->cacheTtl, function() use ($name) {
            return $this->getFrameImage($this->getPassepartoutByName($name));
        });
    }

    public function getFrameImageByName($name) {
        return Cache::remember('api_frame_img_'. $name, $this->cacheTtl, function() use ($name) {
            return $this->getFrameImage($this->getFrameByName($name));
        });
    }

    public function normalizeName($name) {
        return strtoupper(str_replace(' ', '', $name));
    }
}