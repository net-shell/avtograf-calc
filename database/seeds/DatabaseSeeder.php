<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Prices
        $data = (object)[
            'passepartout' => 3,
            'glass' => [
                ['name' => 'Обикновено 2mm', 'price' => 15],
                ['name' => 'Антирефлексно 2mm', 'price' => 30],
                ['name' => 'Вералит/плексиглас 0.75mm', 'price' => 30]
            ],
            'mirror' => 28,
            'back' => 4,
            'hanging' => [
                ['name' => 'Закачалка - малка', 'price' => 0.3],
                ['name' => 'Закачалка - средна', 'price' => 0.5],
                ['name' => 'Закачалка - голяма', 'price' => 1],
                ['name' => 'Въже', 'price' => 1.5]
            ],
            'tapestry' => 5,
            'subframe' => 3
        ];

        DB::table('prices')->insert([
            'name' => 'Май 2017',
            'data' => json_encode($data)
        ]);
    }
}
