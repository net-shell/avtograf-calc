<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Image;

class PreviewController extends Controller
{
    private $order;
    private $api;
    private $cm2px = 37.795275591;

    public function __construct(Request $request) {
        $this->order = json_decode($request->input('order'));
        $this->api = app('Avtograf\API');
        $this->order->frame1 = $this->api->normalizeName($this->order->frame1);
        $this->order->frame2 = $this->api->normalizeName($this->order->frame2);
    }

    public function render()
    {
        if(!$this->order || !$this->order->frame1)
            return;
        $timer = microtime(true);

        // ! Critical optimization:
        // "Geometric scale out" based on Pmax=64cm to make big numbers
        // possible to draw in the average human lifespan; i.e. the larger
        // the size, the more scaled down everything gets. If P < Pmax
        // the image will be rendered in 1:1 scale.
        $scale = ($this->order->width + $this->order->height) / 54;
        // Just don't zoom in please.
        if($scale > 1) {
            $this->cm2px /= $scale;
        }

        // Setup canvas
        $img = Image::canvas($this->order->width * $this->cm2px, $this->order->height * $this->cm2px);
        $img->interlace();
        $userImageArea = [$img->width(), $img->height()];

        // Draw passepartout
        if($this->order->passepartout && $this->order->passepartout->width && $this->order->passepartout->name) {
            $width = round($this->cm2px * $this->order->passepartout->width);
            $img->resizeCanvas($width * 2, $width * 2, 'center', true);
            $imageFile = $this->api->getPassepartoutImageByName($this->order->passepartout->name);
            $passepartoutImage = Image::make($imageFile);
            $passepartoutImage->crop(round($passepartoutImage->width() * 0.7), round($passepartoutImage->height() * 0.8));
            $passepartoutImage->resize($userImageArea[0] + ($width * 2), $userImageArea[1] + ($width * 2));
            $img->insert($passepartoutImage, 'center');
        }

        // Draw user image
        if($this->order->userImage) {
            $userImage = Image::make(public_path('uploads/' . $this->order->userImage));
            $userImage->resize($userImageArea[0], $userImageArea[1]);
            $img->insert($userImage, 'center');
        } else {
            // Draw logo placeholder
            $logo = Image::make(public_path('img/logo.png'));
            $logoWidth = $userImageArea[0] - round($this->cm2px);
            if($logoWidth > 108) {
                $logo->widen($logoWidth, function($constraint) {
                    $constraint->upsize();
                })->greyscale();
                $img->insert($logo, 'center');
            }
        }

        // Draw inner frame
        if($this->order->frame1) {
            $frame1 = $this->api->getFrameByName($this->order->frame1);
            $this->insertFrameImage($frame1, $img);
        }

        // Draw outer frame
        if($this->order->frame2) {
            $frame2 = $this->api->getFrameByName($this->order->frame2);
            $this->insertFrameImage($frame2, $img);
        }

        // Generate image data response
        return $img->response('jpg', 69);
    }

    private function insertFrameImage($frameData, $img) {
        $frameFile = $this->api->getFrameImage($frameData);
        $frameWidthCm = $frameData->width ? $frameData->width * 0.1 : 1.5;

        // Setup frame
        $frameWidth = round($frameWidthCm * $this->cm2px);
        $frame = $this->getFrameSide($img, $frameFile, $frameWidth);

        // Draw the frame
        $img->resizeCanvas($frameWidth * 2, $frameWidth * 2, 'center', true);
        $img->insert($frame, 'top-left');

        // Cut corner mask
        $mask = $this->getFrameMask($frame);
        $mask->flip('v');
        $frame->mask($mask, false);

        $frame->rotate(-90);
        $frame->flip();
        $img->insert($frame, 'top-left');
        $frame->rotate(90);
        $img->insert($frame, 'bottom-left');

        // Cut last corner mask
        $mask = $this->getFrameMask($frame, $frame->width() - $img->height());
        $mask->flip();
        $frame->mask($mask, false);

        $frame->rotate(90);
        $img->insert($frame, 'bottom-right');
    }

    private function getFrameSide($img, $frameFile, $frameWidth) {
        try {
            $frame = $this->getFrameImage($frameFile, $frameWidth);
        } catch(\Exception $e) {
            $frame = false;
            dd($e);
        }
        if(!$frame) {
            $missing = Image::canvas($frameWidth * 10, $frameWidth, '#666666');
            $missing->text($this->order->frame1, $frameWidth, $frameWidth, function($font) use ($frameWidth) {
                $font->file(public_path('fonts/Diplomata.ttf'));
                $font->size($frameWidth);
            });
            return $missing;
        }
        $frameBigSize = max($frame->width(), $frame->height());
        // Repeat frame until the bigger image size is reached
        $frameK = $img->width() > $img->height() ? ceil($img->width() / $frameBigSize) : ceil($img->height() / $frameBigSize);
        $dolly = $this->getFrameImage($frameFile, $frameWidth);
        $yllod = $this->getFrameImage($frameFile, $frameWidth);
        $yllod->flip();
        for($i=0; $i <= ceil($frameK / 2); $i++) {
            $frame->resizeCanvas($frameBigSize * 2, 0, 'left', true);
            // Clone and flip frame image
            $frame->insert($yllod, 'right', $frameBigSize);
            $frame->insert($dolly, 'right');
        }
        $dolly->destroy();
        $yllod->destroy();
        return $frame;
    }

    private function getFrameMask($frame, $offset = 0) {
        $mask = Image::canvas($frame->width(), $frame->height(), '#ffffff');
        $jointPoints = [
            $offset, 0,
            $offset + $frame->height(), 0,
            $offset, $frame->height()
        ];
        $mask->polygon($jointPoints, function($draw) {
            $draw->background('#000000');
        });
        return $mask;
    }

    private function getFrameImage($frameFile, $frameWidth) {
        $frame = Image::make(Image::cache(function($image) use ($frameFile) {
            return $image->make($frameFile);
        }));
        $originalWidth = $frame->width();
        $kW = 0.1;//$frameWidth / 1000;
        $frame->crop(round($originalWidth * (0.4 - $kW)), round($frame->height() * 0.5), round($originalWidth * (0.2 + $kW)), 0);
        $frame->trim('top-left', ['top', 'bottom'], 10);
        $frame->trim('bottom-right', ['bottom'], 60);
        //$frame->limitColors(255, '#ffffff');
        if($frameWidth) {
            $frame->resize(null, $frameWidth, function ($constraint) {
                $constraint->aspectRatio();
            });
        }
        return $frame;
    }
}
